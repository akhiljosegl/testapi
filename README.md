# Dice Game API

### Rules of the Game:
* There is a maximum of 4 players.
* You can enter the actual amount players.
* You can name the different players.
* The first player to get a total sum of 25 is the winner. A player
does not have to get 25 exactly (>=25 is OK).
* To get started the player will need to get 6. If the player gets
1-5 they will then have to wait for their turn before having
another go.
* When finally hitting the number 6 the player will have to throw
again to determine the starting point. Getting a 6 on the first
try will give you 0.
* Each time a player hits number 4, he will get -4 from the total
score.
* If a player hits a 4 after hitting the first 6, they do not get a
negative score but will have to roll another 6 before they start
accumulating points.
* Each time a player hits the number 6 he will then get one extra
throw.

### API Details:

* There are 3 API's in Game controller 
* API's are developed in a way that it can be integrated with any web app or mobile app
* After running the application the API's can be tested using  http://localhost:9090/swagger-ui.html#/ 
* H2 Data base can be accessed using http://localhost:9090/h2/login.jsp Username & Password is availalbe in property file
* Application uses 2 tables PLAYERS and GAME (one row to Game table will be added while startup, Players are to be added manually)
* Easily start the application by running the TestApiApplication.jar or execute the .jar file in a server.

### How to PLAY:

1. Add players using post method http://localhost:9090/vb/game/addPlayer
   sample request {
                    "gameId": 1,
                    "players": [
                      "PHILL",
                      "FRED"
                    ]
                  }
2. Players can roll dice using Post method http://localhost:9090/vb/game/throwDice
   INPUTS : playerId and GameId (1 is default game id)
   
   Response will contain the score dice value and a message saying who is the next player
   
   Sample: 
   {
             "status": true,
             "data": {
               "playerId": null,
               "playerName": "PHILL",
               "score": 0,
               "diceValue": 5,
               "message": "Next Player is : 2"
             },
             "error": null
           }
           
3. Multiple games can be created at the same time and Players can be added to different Games.
   
   Post method http://localhost:9090/vb/game/createGame can be used to create new Game.
   
4. Maximum number of Players is 4 and Winning Score is 25 by Default which can be changed from application.properties if needed. 
5. Application will not allow to start playing without minimum 2 Players



## Thank You