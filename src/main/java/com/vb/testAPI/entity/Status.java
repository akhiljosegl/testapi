package com.vb.testAPI.entity;

public enum Status {
    NOT_STARTED, STARTED, GAME_OVER
}
