package com.vb.testAPI.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TBL_GAME")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status gameStatus;

    @Column(name = "lastPlayer")
    private Long lastPlayer;

    @Column(name = "nextPlayer")
    private Long nextPlayer;

    @Column(name = "maxPlayers")
    private Long maxPayers;

    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL)
    private List<Player> players;

    public Game() {
    }

    public Game(Status gameStatus, Long lastPlayer, Long nextPlayer) {
        this.gameStatus = gameStatus;
        this.lastPlayer = lastPlayer;
        this.nextPlayer = nextPlayer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(Status gameStatus) {
        this.gameStatus = gameStatus;
    }

    public Long getLastPlayer() {
        return lastPlayer;
    }

    public void setLastPlayer(Long lastPlayer) {
        this.lastPlayer = lastPlayer;
    }

    public Long getNextPlayer() {
        return nextPlayer;
    }

    public void setNextPlayer(Long nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    public Long getMaxPayers() {
        return maxPayers;
    }

    public void setMaxPayers(Long maxPayers) {
        this.maxPayers = maxPayers;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
}