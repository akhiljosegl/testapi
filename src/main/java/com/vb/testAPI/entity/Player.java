package com.vb.testAPI.entity;

import javax.persistence.*;

@Entity
@Table(name = "TBL_PLAYER")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "player_name")
    private String playerName;

    @Column(name = "score")
    private Integer score;

    @Column(name = "lastScore")
    private Integer lastScore;

    @Column(name = "isWinner")
    private Boolean isWinner;

    @Column(name = "started")
    private Boolean started;

    @ManyToOne(cascade = CascadeType.ALL)
    private Game game;

    public Player(Long id, String playerName, Integer score, Integer lastScore, Boolean isWinner, Boolean started, Game game) {
        this.id = id;
        this.playerName = playerName;
        this.score = score;
        this.lastScore = lastScore;
        this.isWinner = isWinner;
        this.started = started;
        this.game = game;
    }

    public Player(String playerName, Integer score, Integer lastScore, Boolean isWinner, Boolean started, Game game) {
        this.playerName = playerName;
        this.score = score;
        this.lastScore = lastScore;
        this.isWinner = isWinner;
        this.started = started;
        this.game = game;
    }

    public Player() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Integer getLastScore() {
        return lastScore;
    }

    public void setLastScore(Integer lastScore) {
        this.lastScore = lastScore;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Boolean getWinner() {
        return isWinner;
    }

    public void setWinner(Boolean winner) {
        isWinner = winner;
    }

    public Boolean getStarted() {
        return started;
    }

    public void setStarted(Boolean started) {
        this.started = started;
    }
}