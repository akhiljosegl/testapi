package com.vb.testAPI.controller;

import com.vb.testAPI.exceptions.ConstraintsViolationException;
import com.vb.testAPI.exceptions.GameRuleViolationException;
import com.vb.testAPI.paload.*;
import com.vb.testAPI.service.GameService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("vb/game/")
public class GameController {

    @Autowired
    GameService service;

    @PostMapping("addPlayer")
    @ApiOperation(" Enrolls players to the Game")
    public ResponseEntity<?> add(@RequestBody PlayerRequest players) {
        log.info("Adding Players");
        ResponsePayload player = null;
        try {
            player = service.addPlayer(players);
            return ResponseEntity.ok(new BaseResponse<ResponsePayload>(true, player, null));
        } catch (ConstraintsViolationException e) {
            log.info("Exception while adding players");
            return ResponseEntity.ok(new BaseResponse<ResponsePayload>(false, null, new BaseError(e.getMessage(), 111)));
        }

    }

    @PostMapping("createGame")
    @ApiOperation(" Creates a new Game")
    public ResponseEntity<?> addGame() {
        log.info("Adding New Game");
        return ResponseEntity.ok(new BaseResponse<Message>(true, service.newGame(), null));
    }

    @PostMapping("throwDice")
    @ApiOperation(" throw dice")
    public ResponseEntity<?> throwDice(@RequestBody PlayRequest play) {
        log.info("Rolling Dice");
        ResponsePayload player = null;
        try {
            player = service.throwDice(play);
            return ResponseEntity.ok(new BaseResponse<ResponsePayload>(true, player, null));
        } catch (GameRuleViolationException e) {
            return ResponseEntity.ok(new BaseResponse<ResponsePayload>(false, null, new BaseError(e.getMessage(), 222)));
        }
    }


}
