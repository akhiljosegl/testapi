package com.vb.testAPI.paload;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Message {

    private String message;
}
