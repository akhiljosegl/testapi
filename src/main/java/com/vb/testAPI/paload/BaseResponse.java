package com.vb.testAPI.paload;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse<T> {
    private boolean status;
    private T data;
    private BaseError error;
}
