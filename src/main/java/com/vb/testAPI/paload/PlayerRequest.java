package com.vb.testAPI.paload;

import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlayerRequest {

    private List<String> players;
    private Long gameId;

}
