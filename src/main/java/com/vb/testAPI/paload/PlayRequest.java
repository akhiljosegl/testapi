package com.vb.testAPI.paload;


import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlayRequest {

    private Long playerId;
    private Long gameId;
}
