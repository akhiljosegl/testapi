package com.vb.testAPI.paload;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BaseError {
    String errorMessage;
    int errorCode;
}
