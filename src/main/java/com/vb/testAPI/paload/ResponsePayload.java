package com.vb.testAPI.paload;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponsePayload {

    private Long playerId;
    private String playerName;
    private Integer score;
    private Integer diceValue;
    private String message;

}
