package com.vb.testAPI.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Some rules are violated ...")
public class GameRuleViolationException extends Exception {

    static final long serialVersionUID = -3387516993224229949L;
    public GameRuleViolationException(String message) {
        super(message);
    }

}