package com.vb.testAPI;

import com.vb.testAPI.entity.Game;
import com.vb.testAPI.entity.Status;
import com.vb.testAPI.repository.GameRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class TestApiApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext conAppConfig = SpringApplication.run(TestApiApplication.class, args);
        GameRepository repo = conAppConfig.getBean(GameRepository.class);

        repo.save(new Game(Status.NOT_STARTED, 0L, 0L));
    }


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("vb_test").apiInfo(apiInfo())
                .select().apis(RequestHandlerSelectors.basePackage("com.vb.testAPI.controller"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("VoiceBidge Dice Game services").version("1.0.0").build();
    }


}
