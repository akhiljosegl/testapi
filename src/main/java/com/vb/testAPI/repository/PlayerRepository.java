package com.vb.testAPI.repository;

import com.vb.testAPI.entity.Game;
import com.vb.testAPI.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository
        extends JpaRepository<Player, Long> {


    @Query("SELECT p from Player p where p.game = :game")
    List<Player> findAllbyGame(Game game);

}
