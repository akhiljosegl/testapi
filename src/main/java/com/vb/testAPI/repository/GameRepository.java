package com.vb.testAPI.repository;

import com.vb.testAPI.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository
        extends JpaRepository<Game, Long> {

    @Query("SELECT gm FROM Game gm WHERE id =?1")
    Game findByID(Long gameId);
}
