package com.vb.testAPI.service;

import com.vb.testAPI.entity.Game;
import com.vb.testAPI.entity.Player;
import com.vb.testAPI.entity.Status;
import com.vb.testAPI.exceptions.ConstraintsViolationException;
import com.vb.testAPI.exceptions.GameRuleViolationException;
import com.vb.testAPI.paload.Message;
import com.vb.testAPI.paload.PlayRequest;
import com.vb.testAPI.paload.PlayerRequest;
import com.vb.testAPI.paload.ResponsePayload;
import com.vb.testAPI.repository.GameRepository;
import com.vb.testAPI.repository.PlayerRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;

@Slf4j
@Service
public class GameService {

    @Value("${GAME.RULE.MAX_PLAYERS}")
    private int playerCount;

    @Value("${GAME.RULE.MAX_SCORE}")
    private int maxScore;

    @Value("${GAME.API.DICE}")
    private String rollDiceURI;


    @Autowired
    PlayerRepository playerRepo;

    @Autowired
    GameRepository gameRepo;

    RestTemplate restTemplate = new RestTemplate();

    /**
     * @param names
     * @return Player Count
     * @throws ConstraintsViolationException
     */
    public ResponsePayload addPlayer(PlayerRequest names) throws ConstraintsViolationException {

        log.info("Add Player Service");
        ResponsePayload response = new ResponsePayload();

        Game game = gameRepo.findByID(names.getGameId());
        List<Player> results = playerRepo.findAllbyGame(game);
        if (results.size() < playerCount && names.getPlayers().size() < playerCount) {
            for (String name : names.getPlayers()) {
                if (!name.equals(null) && name.length() > 1) {

                    Player playerObj = playerRepo.save(new Player(name, 0, 0, false, false, game));

                } else {
                    throw new ConstraintsViolationException("Please provide valid Player name");
                }
            }
            response.setMessage(names.getPlayers().size() + " Players Added to Game");
            log.info(names.getPlayers().size() + " Players Added to Game");
            return response;
        } else {
            log.error("More than " + playerCount + " players cannot be added");
            throw new ConstraintsViolationException("More than " + playerCount + " players cannot be added");
        }
    }


    /**
     * @param play
     * @return next Player ID
     * @throws ConstraintsViolationException
     */
    public ResponsePayload throwDice(PlayRequest play) throws GameRuleViolationException {
        log.info("Thow Dice Service");
        ResponsePayload response = new ResponsePayload();

        try {

            Optional<Player> playerObj = playerRepo.findById(play.getPlayerId());
            Game game = gameRepo.getOne(play.getGameId());
            if (playerRepo.findAllbyGame(game).size() < 2) {
                throw new GameRuleViolationException("There should be atleast 2 players in a game");
            }
            game.setGameStatus(Status.STARTED);
            if (playerObj.isPresent()) {
                Player player = playerObj.get();
                int rollscore = rollDice();

                log.info("Player name: " + player.getPlayerName() + "; Total Score: " + player.getScore() + "; Current Value of Dice: " + rollscore);
                switch (rollscore) {
                    case 6:
                        if (!player.getStarted()) {
                            log.info(player.getPlayerName() + " Enrolled in Game " + game.getId());
                            addScore(0, rollscore, player, game, true);
                        } else {
                            addScore(6, rollscore, player, game, true);
                        }
                        break;
                    case 4:
                        if (player.getStarted()) {
                            subScore(4, rollscore, player, game);
                        } else {
                            addScore(0, rollscore, player, game, false);
                        }
                        break;
                    default:
                        if (player.getStarted()) {
                            addScore(rollscore, rollscore, player, game, true);
                        } else {
                            addScore(0, rollscore, player, game, false);
                        }
                }

                if (player.getScore() >= maxScore) {
                    game.setGameStatus(Status.GAME_OVER);
                    player.setWinner(true);
                    playerRepo.saveAndFlush(player);
                    gameRepo.saveAndFlush(game);
                    response.setPlayerName(player.getPlayerName());
                    response.setScore(player.getScore());
                    response.setDiceValue(player.getLastScore());
                    response.setMessage("Game Over. Winner is " + player.getPlayerName());
                    log.info("Game : " + game.getId() + " is Over, Winner is - " + player.getPlayerName());
                    return response;
                } else {
                    response.setPlayerName(player.getPlayerName());
                    response.setScore(player.getScore());
                    response.setDiceValue(player.getLastScore());
                    response.setMessage("Next Player is : " + game.getNextPlayer());
                    playerRepo.saveAndFlush(player);
                    gameRepo.saveAndFlush(game);
                    log.info("Throw Dice Response successful");
                    return response;
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new GameRuleViolationException(e.getMessage());
        }
        return response;
    }

    /**
     * @param i,         score to be incremented
     * @param rollscore, Current dice value
     * @param player
     * @param game
     * @param started,   Game status
     */
    private void addScore(int i, int rollscore, Player player, Game game, boolean started) {
        player.setStarted(started);
        player.setScore(player.getScore() + i);
        player.setLastScore(rollscore);
        game.setLastPlayer(player.getId());
        if (rollscore == 6) game.setNextPlayer(player.getId());
        else game.setNextPlayer(getNextPlayer(player.getId(), game));
    }

    /**
     * @param i,        score to be reduced
     * @param rollscore
     * @param player
     * @param game
     */
    private void subScore(int i, int rollscore, Player player, Game game) {

        player.setScore(player.getScore() - i);
        player.setLastScore(rollscore);
        game.setLastPlayer(player.getId());
        game.setNextPlayer(getNextPlayer(player.getId(), game));

        if (player.getScore() < 0) {
            player.setStarted(false);
        }
    }

    /**
     * @param playerId, Current Player
     * @param gameId
     * @return playerId, Next player
     */
    private Long getNextPlayer(Long playerId, Game gameId) {

        List<Player> allPlayers = playerRepo.findAllbyGame(gameId);
        OptionalInt optionalInt = IntStream.range(0, allPlayers.size()).filter(index -> allPlayers.get(index).getId().longValue() == playerId.longValue()).findFirst();


        if (optionalInt.isEmpty() || optionalInt.getAsInt() == allPlayers.size() - 1) {
            return allPlayers.get(0).getId();
        } else {
            return allPlayers.get(optionalInt.getAsInt() + 1).getId();
        }
    }

    private int rollDice() {
        try {
            log.info("Consuming Dice Score API...");
            String result = restTemplate.getForObject(rollDiceURI, String.class);
            JSONObject jobj = new JSONObject(result);
            return jobj.getInt("score");
        } catch (Exception e) {
            log.error("Exception in Consuming Dice Score API " + e.getMessage());
            return 0;
        }
    }


    public Message newGame() {
        Game game = gameRepo.save(new Game(Status.NOT_STARTED, 0L, 0L));
        log.info("New Game Creation GameID " + game.getId());
        return new Message("New Game Created, ID: " + game.getId());
    }
}
