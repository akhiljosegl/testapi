package com.vb.testAPI.service;


import com.vb.testAPI.entity.Game;
import com.vb.testAPI.entity.Player;
import com.vb.testAPI.entity.Status;
import com.vb.testAPI.exceptions.ConstraintsViolationException;
import com.vb.testAPI.exceptions.GameRuleViolationException;
import com.vb.testAPI.paload.PlayRequest;
import com.vb.testAPI.paload.PlayerRequest;
import com.vb.testAPI.paload.ResponsePayload;
import com.vb.testAPI.repository.GameRepository;
import com.vb.testAPI.repository.PlayerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ActiveProfiles("function")
@WebMvcTest(value = GameService.class)
@AutoConfigureMockMvc(addFilters = false)
public class GameServiceTest {


    @MockBean
    GameRepository gameRepository;

    @MockBean
    PlayerRepository playerRepository;


    @Autowired
    GameService gameService;


    @Test
    @WithMockUser
    public void addPlayer_Success() throws Exception {

        List<String> pl = new ArrayList<>();
        pl.add("Phil");
        pl.add("Euwan");
        PlayerRequest req = new PlayerRequest();
        req.setGameId(1l);
        req.setPlayers(pl);

        Game game = new Game();
        game.setId(1l);
        game.setNextPlayer(0l);
        game.setGameStatus(Status.STARTED);
        game.setLastPlayer(0l);

        Player playerOb = new Player("Phil", 0, 0, false, false, game);
        List<Player> plrList = new ArrayList<>();
        plrList.add(new Player("John", 0, 0, false, false, game));
        plrList.add(new Player("Fred", 0, 0, false, false, game));

        when(gameRepository.findByID(anyLong())).thenReturn(game);
        when(playerRepository.findAllbyGame(any(Game.class))).thenReturn(plrList);

        when(playerRepository.save(any(Player.class))).thenReturn(playerOb);

        ResponsePayload response = gameService.addPlayer(req);
        assertNotNull(response.getMessage());
        assertEquals("2 Players Added to Game", response.getMessage());
    }

    @Test
    @WithMockUser
    public void addPlayer_fail() throws Exception {

        List<String> pl = new ArrayList<>();
        pl.add("Phil");
        pl.add("Euwan");
        PlayerRequest req = new PlayerRequest();
        req.setGameId(1l);
        req.setPlayers(pl);

        Game game = new Game();
        game.setId(1l);
        game.setNextPlayer(0l);
        game.setGameStatus(Status.STARTED);
        game.setLastPlayer(0l);

        Player playerOb = new Player("Phil", 0, 0, false, false, game);
        List<Player> plrList = new ArrayList<>();
        plrList.add(new Player("John", 0, 0, false, false, game));
        plrList.add(new Player("Fred", 0, 0, false, false, game));
        plrList.add(new Player("Mary", 0, 0, false, false, game));
        plrList.add(new Player("Jos", 0, 0, false, false, game));
        plrList.add(new Player("Fil", 0, 0, false, false, game));

        when(gameRepository.findByID(anyLong())).thenReturn(game);
        when(playerRepository.findAllbyGame(any(Game.class))).thenReturn(plrList);

        when(playerRepository.save(any(Player.class))).thenReturn(playerOb);


        assertThrows(ConstraintsViolationException.class
        ,()->gameService.addPlayer(req));
    }




    @Test
    @WithMockUser
    public void throwDice_Success() throws Exception {

        List<String> pl = new ArrayList<>();
        pl.add("Phil");
        pl.add("Euwan");
        PlayRequest req = new PlayRequest();
        req.setGameId(1l);
        req.setPlayerId(1l);

        Game game = new Game();
        game.setId(1l);
        game.setNextPlayer(0l);
        game.setGameStatus(Status.STARTED);
        game.setLastPlayer(0l);
        Player playerOb = new Player(1l,"Phil", 0, 0, false, false, game);
        Optional<Player> playerObj = Optional.of(playerOb);

        List<Player> plrList = new ArrayList<>();
        plrList.add(new Player(2l,"John", 0, 0, false, false, game));
        plrList.add(new Player(3l,"Fred", 0, 0, false, false, game));


        when(playerRepository.findById(anyLong())).thenReturn(playerObj);
        when(gameRepository.getOne(anyLong())).thenReturn(game);
        when(playerRepository.findAllbyGame(any(Game.class))).thenReturn(plrList);
        when(playerRepository.save(any(Player.class))).thenReturn(playerOb);

        when(playerRepository.saveAndFlush(playerOb)).thenReturn(playerOb);
        when(gameRepository.saveAndFlush(game)).thenReturn(game);

        ResponsePayload response = gameService.throwDice(req);
        assertNotNull(response.getMessage());
        assertEquals("Next Player is : 2", response.getMessage());
    }


    @Test
    @WithMockUser
    public void throwDice_Fail() throws Exception {

        List<String> pl = new ArrayList<>();
        pl.add("Phil");
        pl.add("Euwan");
        PlayRequest req = new PlayRequest();
        req.setGameId(1l);
        req.setPlayerId(1l);

        Game game = new Game();
        game.setId(1l);
        game.setNextPlayer(0l);
        game.setGameStatus(Status.STARTED);
        game.setLastPlayer(0l);
        Player playerOb = new Player(1l,"Phil", 0, 0, false, false, game);
        Optional<Player> playerObj = Optional.of(playerOb);

        List<Player> plrList = new ArrayList<>();
        plrList.add(new Player(2l,"John", 0, 0, false, false, game));


        when(playerRepository.findById(anyLong())).thenReturn(playerObj);
        when(gameRepository.getOne(anyLong())).thenReturn(game);
        when(playerRepository.findAllbyGame(any(Game.class))).thenReturn(plrList);
        when(playerRepository.save(any(Player.class))).thenReturn(playerOb);

        when(playerRepository.saveAndFlush(playerOb)).thenReturn(playerOb);
        when(gameRepository.saveAndFlush(game)).thenReturn(game);

        assertThrows(GameRuleViolationException.class
                ,()->gameService.throwDice(req));
    }

}
