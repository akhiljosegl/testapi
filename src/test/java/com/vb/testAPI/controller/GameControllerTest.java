package com.vb.testAPI.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.vb.testAPI.exceptions.ConstraintsViolationException;
import com.vb.testAPI.exceptions.GameRuleViolationException;
import com.vb.testAPI.paload.Message;
import com.vb.testAPI.paload.PlayRequest;
import com.vb.testAPI.paload.PlayerRequest;
import com.vb.testAPI.paload.ResponsePayload;
import com.vb.testAPI.service.GameService;
import com.vb.testAPI.service.GameServiceTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("function")
@WebMvcTest(value = GameController.class)
@AutoConfigureMockMvc(addFilters = false)
public class GameControllerTest {

    @MockBean
    GameService gameService;


    @Autowired
    private MockMvc mockMvc;


    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @WithMockUser
    void add_success() throws Exception {
        ResponsePayload player = new ResponsePayload();

        when(gameService.addPlayer(any(PlayerRequest.class))).thenReturn(player);
        mockMvc.perform(post("/vb/game/addPlayer")
                .content(asJsonString(new PlayerRequest()))
                .characterEncoding("UTF-8")
                .contentType("application/json"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(true));
    }

    @Test
    @WithMockUser
    void add_fail() throws Exception {
        when(gameService.addPlayer(any(PlayerRequest.class))).thenThrow(new ConstraintsViolationException("Invalid Game"));
        mockMvc.perform(post("/vb/game/addPlayer")
                .content(asJsonString(new PlayRequest()))
                .characterEncoding("UTF-8")
                .contentType("application/json"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.status").value(false));
    }

    @Test
    @WithMockUser
    void addGame_success() throws Exception {
        Message msg = new Message();
        msg.setMessage("");
        when(gameService.newGame()).thenReturn(msg);
        mockMvc.perform(post("/vb/game/createGame")
                .characterEncoding("UTF-8")
                .contentType("application/json"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(true));
    }

    @Test
    @WithMockUser
    void throwDice_success() throws Exception {
        ResponsePayload player = new ResponsePayload();

        when(gameService.throwDice(any(PlayRequest.class))).thenReturn(player);
        mockMvc.perform(post("/vb/game/throwDice")
                .content(asJsonString(new PlayerRequest()))
                .characterEncoding("UTF-8")
                .contentType("application/json"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(true));
    }

    @Test
    @WithMockUser
    void throwDice_fail() throws Exception {
        when(gameService.throwDice(any(PlayRequest.class))).thenThrow(new GameRuleViolationException("Invalid Game"));
        mockMvc.perform(post("/vb/game/throwDice")
                .content(asJsonString(new PlayerRequest()))
                .characterEncoding("UTF-8")
                .contentType("application/json"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(false));
    }

}
